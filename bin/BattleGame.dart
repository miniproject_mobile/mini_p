// ignore: file_names
// ignore_for_file: non_constant_identifier_names, unnecessary_new

import 'dart:ffi';
import 'dart:io';
import 'dart:math';

class Board {
  late List<List<bool>> isFull;
  late List<List<Char>> symbols;
  late List<List<bool>> isShot;
  late List<List<Char>> shootSymbols;
  static const int N = 10;

  Board() {
    for (int i = 0; i < N; i++) {
      for (int j = 0; j < N; j++) {
        symbols[i][j] = ' '.codeUnitAt(0) as Char;
        shootSymbols[i][j] = ' '.codeUnitAt(0) as Char;
        isShot[i][j] = false;
        isFull[i][j] = false;
      }
    }
  }

  // ships symbols
  List<List<Char>> getSymbols() {
    return symbols;
  }

  // that is for shoot on the target
  List<List<Char>> getShootSymbols() {
    return shootSymbols;
  }

  // for checking the board is full or not
  List<List<bool>> getIsFull() {
    return isFull;
  }

  // for checking the board is shot or not
  List<List<bool>> getIsShot() {
    return isShot;
  }

  // specifies one dimension of the board
  int getN() {
    return N;
  }
}

class Shippart {
  int x = 0;
  int y = 0;
  bool isBroken = false;

  Shippart(int size) {
    x = -1;
    y = -1;
  }

  int getX() {
    return x;
  }

  void setX(int x) {
    this.x = x;
  }

  int getY() {
    return y;
  }

  void setY(int y) {
    this.y = y;
  }

  bool isbroken() {
    return isBroken;
  }

  void setBroken(bool broken) {
    isBroken = broken;
  }
}

class Ship {
  int size;
  late List<Shippart> parts;
  late bool isBurst;
  // size specifies size of the ship
  Ship(this.size) {
    size = size;
    //parts = new ShipPart[size];
    for (int i = 0; i < size; i++) {
      parts[i] = Shippart(size);
    }
    isBurst = false;
  }

  Shippart? get fill => null;

  // parts of a ship
  List<Shippart> getParts() {
    return parts;
  }

  void buildShip(Board board) {
    String? input = stdin.readLineSync();
    print("คุณต้องการป้อนแถวแนวนอนหรือไม่(y/n)");
    String ans = input.toString();
    if (ans == ('y')) {
      print("กรุณาใส่แถวแนวนอนที่ต้องการวางเรือ");
      int x = stdin.readByteSync();
      while (x < 0 || x > 9) {
        print("กรุณาใส่แถวแนวนอนอีกครั้ง!!!");
        x = stdin.readByteSync();
      }
      for (int i = 0; i < size; i++) {
        //print("\n กรุณาใส่ตำแหน่งของชิ้นส่วนเรือตอนที่ " + (i + 1));
        print("เลือกแถวแนวตั้ง: ");
        int y = 0; //= input.nextInt();
        while (y < 0 || y > 9) {
          //print("กรุณาใส่ตำแหน่งของชิ้นส่วนเรือตอนที่" + (i + 1) + ", อีกครั้ง");
          print("เลือกแถวแนวตั้ง: ");
          y = stdin.readByteSync();
        }
        for (int j = 0; j <= i; j++) {
          if ((parts[j].getX() == x && parts[j].getY() == y) ||
              board.getIsFull()[x][y]) {
            print(
                "\n กรุณาใส่ตำแหน่งของชิ้นส่วนเรือตอนที่ "); // + (i + 1) + ", อีกครั้ง"
            print("เลือกแถวแนวตั้ง: ");
            int y = stdin.readByteSync();
            while (y < 0 || y > 9) {
              print(
                  "\n กรุณาใส่ตำแหน่งของชิ้นส่วนเรือตอนที่ "); // + (i + 1) + ", อีกครั้ง"
              print("เลือกแถวแนวตั้ง: ");
              y = stdin.readByteSync();
            }
            j = -1;
          }
        }
        parts[i].setX(x);
        parts[i].setY(y);
      }
    } else {
      print("ถ้าคุณต้องการใส่ลงในแถวแนวตั้ง, กรุณาใส่ตัวเลขแถวแนวตั้ง:");
      print("กรุณาป้อนใส่แถวแนวตั้ง:");
      int y = stdin.readByteSync();
      while (y < 0 || y > 9) {
        print("กรุณาป้อนแถวแนวตั้ง, อีกครั้ง:");
        y = stdin.readByteSync();
      }
      for (int i = 0; i < size; i++) {
        print("ใส่ชิ้นส่วนตำแหน่งเรือตอน");
        print("ป้อนแถวแนวตั้ง: ");
        int x = stdin.readByteSync();
        while (x < 0 || x > 9) {
          print("กรุณาใส่ตำแหน่งของชิ้นส่วนเรือตอนที่ ");
          print("ป้อนแถวแนวตั้ง: ");
          x = stdin.readByteSync();
        }
        for (int j = 0; j <= i; j++) {
          if ((parts[j].getX() == x && parts[j].getY() == y) ||
              board.getIsFull()[x][y]) {
            //print("ใส่ชิ้นส่วนตำแหน่งเรือตอน " + (i + 1) + ", อีกครั้ง");
            print("ป้อนแถวแนวตั้ง: ");
            int x = stdin.readByteSync();
            while (x < 0 || x > 9) {
              //print("ใส่ชิ้นส่วนตำแหน่งเรือตอน " + (i + 1) + ", อีกครั้ง");
              print("ป้อนแถวแนวตั้ง: ");
              x = stdin.readByteSync();
            }
            j = -1;
          }
        }
        parts[i].setX(x);
        parts[i].setY(y);
      }
    }
  }

  void buildAIShip(Board board) {
    // ignore: unnecessary_new
    Random random = new Random();
    int samerow = random.nextInt(2);
    if (samerow == 0) {
      int x = random.nextInt(10);
      int y = random.nextInt(10 - size + 1);
      for (int i = 0; i < size; i++) {
        if ((parts[i].getX() == x && parts[i].getY() == y) ||
            board.getIsFull()[x][y]) {
          x = random.nextInt(10);
          y = random.nextInt(10 - size + 1);
          for (int j = 0; j <= i; j++) {
            parts[i].setX(-1);
            parts[i].setY(-1);
          }
          i = -1;
          continue;
        }
        parts[i].setX(x);
        parts[i].setY(y++);
      }
    } else {
      int x = random.nextInt(10);
      int y = random.nextInt(10 - size + 1);
      for (int i = 0; i < size; i++) {
        if ((parts[i].getX() == x && parts[i].getY() == y) ||
            board.getIsFull()[x][y]) {
          x = random.nextInt(10);
          y = random.nextInt(10 - size + 1);
          for (int j = 0; j <= i; j++) {
            parts[i].setX(-1);
            parts[i].setY(-1);
          }
          i = -1;
          continue;
        }
        parts[i].setX(x++);
        parts[i].setY(y);
      }
    }
  }

  bool partConnect() {
    bool xDirection = true;
    int xPos = parts[0].getX();
    int minx = parts[0].getX();
    int maxx = parts[0].getX();
    for (int i = 0; i < size; i++) {
      if (parts[i].getX() != xPos) {
        xDirection = false;
      }
      minx = parts[i].getX() < minx ? parts[i].getX() : minx;
      maxx = parts[i].getX() > maxx ? parts[i].getX() : maxx;
    }
    int miny = parts[0].getY();
    int maxy = parts[0].getY();
    for (int i = 0; i < size; i++) {
      miny = parts[i].getY() < miny ? parts[i].getY() : miny;
      maxy = parts[i].getY() > maxy ? parts[i].getY() : maxy;
    }
    if (xDirection) {
      return maxy - miny + 1 == size;
    } else {
      return maxx - minx + 1 == size;
    }
  }

  void formantShip() {
    for (int i = 0; i < size; i++) {
      parts[i].setX(-1);
      parts[i].setY(-1);
    }
  }

  void putShipInBoard(Board board) {
    for (int i = 0; i < size; i++) {
      int x = parts[i].getX();
      int y = parts[i].getY();
      if (!board.getIsFull()[x][y]) {
        if (!parts[i].isBroken) {
          board.getSymbols()[x][y] = 'O' as Char;
        } else {
          board.getSymbols()[x][y] = 'X' as Char;
        }
        board.getIsFull()[x][y] = true;
      } else {
        print("ตำแหน่งเต็มแล้ว");
      }
    }
  }

  void upShipInBoard(Board board) {
    for (int i = 0; i < size; i++) {
      int x = parts[i].getX();
      int y = parts[i].getY();
      if (!parts[i].isBroken) {
        board.getSymbols()[x][y] = 'O' as Char;
      } else {
        board.getSymbols()[x][y] = 'X' as Char;
      }
    }
  }

  bool isburst() {
    bool result = true;
    for (int i = 0; i < size; i++) {
      result = result && parts[i].isBroken;
    }
    isBurst = result;
    return isBurst;
  }
}

abstract class Player {
  late Board board;
  late List<Ship> ships;
  late Player opponent;
  late String id;
  static final int numOfShip = 6;

  Player() {
    // ignore: unnecessary_new
    board = new Board();
    // ignore: unnecessary_new
    ships = new Ship(numOfShip) as List<Ship>;
    id = '';
  }
  //board of a player
  Board getBoard() {
    return board;
  }

  //ships of a player
  List<Ship> getShips() {
    return ships;
  }

  void setOpponent(Player opponent) {
    this.opponent = opponent;
  }

  Player getOpponent() {
    return opponent;
  }

  String getId() {
    return id;
  }

  void setId(String id) {
    this.id = id;
  }

  void upShipsInBoard() {
    for (Ship ship in ships) {
      ship.upShipInBoard(board);
    }
  }

  void drawTable() {
    upShipsInBoard();
    print("###########################################" "       " +
        "###########################################");
    print("############### ตารางแมพของคุณ #################" "       " +
        "############# ตารางแมพของศัตรู ###############");
    print("###########################################" "       " +
        "###########################################");
    print("  | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |" "       " +
        "  | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |");
    print("--|---+---+---+---+---+---+---+---+---+---+" "       " +
        "--|---+---+---+---+---+---+---+---+---+---+");
    for (int i = 0; i < board.getN(); i++) {
      //print(i + "|");
      for (int j = 0; j < board.getN(); j++) {
        // ignore: unrelated_type_equality_checks
        if (getBoard().getSymbols()[i][j] == 'O') {
          print("\u001B[44m\u001B[30m O \u001B[0m|");
          // ignore: unrelated_type_equality_checks
        } else if (getBoard().getSymbols()[i][j] == 'X') {
          //System.out.print(" " + "\033[31m#\033[0m" + " |");
          print("\u001B[41m\u001B[30m X \u001B[0m|");
        } else {
          // ignore: prefer_interpolation_to_compose_strings
          //print(" " + getBoard().getSymbols()[i][j] + "|");
        }
      }

      //print("${"       " + i!} |");
      for (int j = 0; j < board.getN(); j++) {
        // ignore: unrelated_type_equality_checks
        if (opponent.getBoard().getShootSymbols()[i][j] == 'U') {
          //System.out.print(" " + "\033[32m&\033[0m" + " |");
          print("\u001B[42m\u001B[30m U \u001B[0m|");
          // ignore: unrelated_type_equality_checks
        } else if (opponent.getBoard().getShootSymbols()[i][j] == 'X') {
          //System.out.print(" " + "\033[33mX\033[0m" + " |");
          print("\u001B[43m\u001B[30m X \u001B[0m|");
        } else {
          //print(" " + opponent.getBoard().getShootSymbols()[i]![j] + "|");
        }
      }
      print(
          "--|---+---+---+---+---+---+---+---+---+---+       --|---+---+---+---+---+---+---+---+---+---+");
    }

    print(drawTable);
  }

  Shippart matchPoint(int x, int y) {
    for (int i = 0; i < numOfShip; i++) {
      for (int j = 0; j < ships[i].getParts().length; j++) {
        if (ships[i].getParts()[j].getX() == x &&
            ships[i].getParts()[j].getY() == y) {
          return ships[i].getParts()[j];
        }
      }
    }
    print("ไม่มีชิ้นส่วนเรือในการแข่งขัน");
    throw RuntimeException("ไม่มีชิ้นส่วนเรือในการแข่งขัน");
  }

  void randomize(List<int> xy) {
    // ignore: unnecessary_new
    Random rand = new Random();
    int x = xy[0];
    int y = xy[1];
    if (x >= 1 && x <= 8 && y >= 1 && y <= 8) {
      x = x - 1 + rand.nextInt(3);
      y = y - 1 + rand.nextInt(3);
    } else if (x == 0 && y == 0) {
      x += rand.nextInt(2);
      y += rand.nextInt(2);
    } else if (x == 9 && y == 9) {
      x = x - 1 + rand.nextInt(2);
      y = y - 1 + rand.nextInt(2);
    } else if (x == 9 && y == 0) {
      x = x - 1 + rand.nextInt(2);
      y = y + rand.nextInt(2);
    } else if (x == 0 && y == 9) {
      x = x + rand.nextInt(2);
      y = y - 1 + rand.nextInt(2);
    } else if (x == 0) {
      x += rand.nextInt(2);
      y = y - 1 + rand.nextInt(3);
    } else if (x == 9) {
      x = x - 1 + rand.nextInt(2);
      y = y - 1 + rand.nextInt(3);
    } else if (y == 0) {
      x = x - 1 + rand.nextInt(3);
      y = y + rand.nextInt(2);
    } else if (y == 9) {
      x = x - 1 + rand.nextInt(3);
      y = y - 1 + rand.nextInt(2);
    }
    xy[0] = x;
    xy[1] = y;
  }

  bool isWinner() {
    bool result = true;
    for (int i = 0; i < numOfShip; i++) {
      result = result && getOpponent().getShips()[i].isburst();
    }
    return result;
  }

  RuntimeException(String s) {}

  void setupShips() {}

  bool shoot(bool isExact) {
    return true;
  }
}

class HumanPlayer extends Player {
  HumanPlayer() {
    super.board;
    setId("ผู้เล่น:Player");
  }

  void setupShips() {
    // ignore: unused_local_variable
    String? input = stdin.readLineSync();
    for (int i = 0; i < Player.numOfShip; i++) {
      // ignore: prefer_interpolation_to_compose_strings
      print("กรุณาระบุขนาดเรือของคุณที่ต้องการ(ขนาด2-5)");
      int size = stdin.readByteSync();
      if (size >= 2 && size <= 5) {
        ships[i] = Ship(size);
        ships[i].buildShip(board);
        --i;
      }
    }
  }

  bool shoot(bool isExact) {
    int x = 0;
    int y = 0;
    String? input = stdin.readLineSync();
    List<int> xy = [2];
    if (!isExact) {
      do {
        xy[0] = x;
        xy[1] = y;
        randomize(xy);
      } while (opponent.getBoard().getIsShot()[xy[0]][xy[1]]);
      x = xy[0];
      y = xy[1];
    }

    opponent.getBoard().getIsShot()[x][y] = true;
    if (opponent.getBoard().getIsFull()[x][y]) {
      opponent.getBoard().getShootSymbols()[x][y] = 'O' as Char;
      opponent.matchPoint(x, y).setBroken(true);
      return true;
    } else {
      opponent.getBoard().getShootSymbols()[x][y] = 'X' as Char;
      return false;
    }
  }
}

class AIPlayer extends Player {
  AIPlayer() {
    super.board;
    setId("คอมพิวเตอร์:AI");
  }
  void setupShips() {
    // ignore: unnecessary_new
    Random rand = new Random();
    for (int i = 0; i < Player.numOfShip; i++) {
      int size = 2 + rand.nextInt(4);
      // ignore: unnecessary_new
      ships[i] = new Ship(size);
      ships[i].buildAIShip(board);
      ships[i].putShipInBoard(board);
    }
  }

  bool shoot(bool isExact) {
    int x = 0;
    int y = 0;

    List<int> xy = 2 as List<int>;
    Random rand = Random();
    do {
      x = rand.nextInt(10);
      y = rand.nextInt(10);
      xy[0] = x;
      xy[1] = y;
      chooser(xy);
    } while (opponent.getBoard().getIsShot()[xy[0]][xy[1]]);
    x = xy[0];
    y = xy[1];

    if (!isExact) {
      do {
        xy[0] = x;
        xy[1] = y;
        randomize(xy);
      } while (opponent.getBoard().getIsShot()[xy[0]][xy[1]]);
      x = xy[0];
      y = xy[1];
    }

    opponent.getBoard().getIsShot()[x][y] = true;
    if (opponent.getBoard().getIsFull()[x][y]) {
      opponent.getBoard().getShootSymbols()[x][y] = 'O' as Char;
      opponent.matchPoint(x, y).setBroken(true);
      return true;
    } else {
      opponent.getBoard().getShootSymbols()[x][y] = 'X' as Char;
      return false;
    }
  }

  void chooser(List<int> xy) {
    for (int i = 0; i < board.getN(); i++) {
      for (int j = 0; j < board.getN(); j++) {
        // ignore: unrelated_type_equality_checks
        if (opponent.getBoard().getShootSymbols()[i][j] == '&') {
          if (i >= 1 && i <= 8 && j >= 1 && j <= 8) {
            if (!opponent.getBoard().getIsShot()[i + 1][j]) {
              xy[0] = i + 1;
              xy[1] = j;
            } else if (!opponent.getBoard().getIsShot()[i][j + 1]) {
              xy[0] = i;
              xy[1] = j + 1;
            } else if (!opponent.getBoard().getIsShot()[i - 1][j]) {
              xy[0] = i - 1;
              xy[1] = j;
            } else if (!opponent.getBoard().getIsShot()[i][j - 1]) {
              xy[0] = i;
              xy[1] = j - 1;
            }
          } else if (i == 0 && j == 0) {
            if (!opponent.getBoard().getIsShot()[i + 1][j]) {
              xy[0] = i + 1;
              xy[1] = j;
            } else if (!opponent.getBoard().getIsShot()[i][j + 1]) {
              xy[0] = i;
              xy[1] = j + 1;
            }
          } else if (i == 9 && j == 9) {
            if (!opponent.getBoard().getIsShot()[i - 1][j]) {
              xy[0] = i - 1;
              xy[1] = j;
            } else if (!opponent.getBoard().getIsShot()[i][j - 1]) {
              xy[0] = i;
              xy[1] = j - 1;
            }
          } else if (i == 9 && j == 0) {
            if (!opponent.getBoard().getIsShot()[i - 1][j]) {
              xy[0] = i - 1;
              xy[1] = j;
            } else if (!opponent.getBoard().getIsShot()[i][j + 1]) {
              xy[0] = i;
              xy[1] = j + 1;
            }
          } else if (i == 0 && j == 9) {
            if (!opponent.getBoard().getIsShot()[i + 1][j]) {
              xy[0] = i + 1;
              xy[1] = j;
            } else if (!opponent.getBoard().getIsShot()[i][j - 1]) {
              xy[0] = i;
              xy[1] = j - 1;
            }
          } else if (i == 0) {
            if (!opponent.getBoard().getIsShot()[i + 1][j]) {
              xy[0] = i + 1;
              xy[1] = j;
            } else if (!opponent.getBoard().getIsShot()[i][j + 1]) {
              xy[0] = i;
              xy[1] = j + 1;
            } else if (!opponent.getBoard().getIsShot()[i][j - 1]) {
              xy[0] = i;
              xy[1] = j - 1;
            }
          } else if (i == 9) {
            if (!opponent.getBoard().getIsShot()[i][j + 1]) {
              xy[0] = i;
              xy[1] = j + 1;
            } else if (!opponent.getBoard().getIsShot()[i][j - 1]) {
              xy[0] = i;
              xy[1] = j - 1;
            } else if (!opponent.getBoard().getIsShot()[i - 1][j]) {
              xy[0] = i - 1;
              xy[1] = j;
            }
          } else if (j == 0) {
            if (!opponent.getBoard().getIsShot()[i + 1][j]) {
              xy[0] = i + 1;
              xy[1] = j;
            } else if (!opponent.getBoard().getIsShot()[i - 1][j]) {
              xy[0] = i - 1;
              xy[1] = j;
            } else if (!opponent.getBoard().getIsShot()[i][j + 1]) {
              xy[0] = i;
              xy[1] = j + 1;
            }
          } else if (j == 9) {
            if (!opponent.getBoard().getIsShot()[i + 1][j]) {
              xy[0] = i + 1;
              xy[1] = j;
            } else if (!opponent.getBoard().getIsShot()[i - 1][j]) {
              xy[0] = i - 1;
              xy[1] = j;
            } else if (!opponent.getBoard().getIsShot()[i][j - 1]) {
              xy[0] = i;
              xy[1] = j - 1;
            }
          }
        }
      }
    }
  }
}

class BattleshipGame extends Player {
  late Player playerA;
  late Player playerB;
  BattleshipGame() {
    playerA = new HumanPlayer();
  }
  void startGame() {
    // ignore: unused_local_variable
    String? input = stdin.readLineSync();
    print("ต้องการเล่นคนเดียวหรือไม่(y/n)");
    String? ans = stdin.readLineSync();
    bool isComputer = false;
    if (ans == ("y")) {
      isComputer = true;
      playerB = new AIPlayer();
    } else {
      playerB = new HumanPlayer();
    }
    playerA.setOpponent(playerB);
    playerB.setOpponent(playerA);
    print("คุณต้องการยิงเองหรือไม่(y/n)");
    ans = stdin.readLineSync();
    bool isExact = false;
    if (ans == ("y")) {
      isExact = true;
    }
    print("ผู้เล่นคนที่ 1:กรุณาป้อนชื่อ: ");
    String? playerAId = stdin.readLineSync();
    playerA.setId(playerAId!);
    playerA.setupShips();

    if (!isComputer) {
      print("ผู้เล่นคนที่ 2:กรุณาป้อนชื่อ: ");
      String? playerBId = stdin.readLineSync();
      playerB.setId(playerBId!);
    }
    playerB.setupShips();

    while (true) {
      playerA.drawTable();
      if (ifTurnProcessEndGame(playerA, isExact)) break;

      if (!isComputer) playerB.drawTable();
      if (ifTurnProcessEndGame(playerB, isExact)) break;
    }
  }

  bool ifEndGame() {
    if (playerA.isWinner()) {
      print("${playerA.getId()} ชนะแล้ว *_*");
      return true;
    }
    if (playerB.isWinner()) {
      print("${playerB.getId()} ชนะแล้ว *_*");
      return true;
    }
    return false;
  }

  bool ifTurnProcessEndGame(Player player, bool isExact) {
    while (true) {
      if (ifEndGame()) {
        return true;
      }
      if (player is HumanPlayer) print(player.getId() + "ตาคุณแล้ว");
      if (!player.shoot(isExact)) {
        return false;
      }
      if (player is HumanPlayer) player.drawTable();
    }
  }
}

void main() {
  print("Battleship Game: ");
  BattleshipGame game = new BattleshipGame();
  game.startGame();
}
